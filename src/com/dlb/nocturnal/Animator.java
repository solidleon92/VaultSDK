package com.dlb.nocturnal;

public abstract class Animator {

    private double elapsed;
    private double duration;
    private boolean finished;
    private Runnable finishCallback;

    public Animator(double duration, Runnable finishCallback) {
        this.duration = duration;
        this.finishCallback = finishCallback;
    }

    public boolean isFinished() {
        return finished;
    }

    public void update(double delta) {
        if (!isFinished()) {
            elapsed += delta;
            if (elapsed >= duration) {
                elapsed = duration;
                finished = true;
                finished();
                if (finishCallback != null)
                    finishCallback.run();
            } else {
                animationUpdate(elapsed, duration);
            }
        }
    }

    protected abstract void animationUpdate(double elapsed, double duration);

    protected abstract void finished();
}
