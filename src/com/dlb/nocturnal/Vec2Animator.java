package com.dlb.nocturnal;

import com.dlb.nocturnal.Animator;
import com.dlb.oxygen.extras.Interpolate;
import com.dlb.oxygen.extras.LinearInterpolation;
import com.dlb.oxygen.extras.Vec2;

public class Vec2Animator extends Animator {

    private Vec2 toAnimate;
    private Vec2 start;
    private Vec2 stop;
    private Interpolate interpolate = new LinearInterpolation();

    public Vec2Animator(Vec2 toAnimate, Vec2 start, Vec2 stop, double seconds, Runnable finishCallback) {
        super(seconds, finishCallback);
        this.toAnimate = toAnimate;
        this.start = start;
        this.stop = stop;
    }

    @Override
    protected void animationUpdate(double elapsed, double duration) {
        toAnimate.x = interpolate.interpolate(elapsed, 0, duration, start.x, stop.x);
        toAnimate.y = interpolate.interpolate(elapsed, 0, duration, start.y, stop.y);
    }

    @Override
    protected void finished() {
        toAnimate.x = stop.x;
        toAnimate.y = stop.y;

    }
}
