package com.dlb.nocturnal;

import com.dlb.oxygen.extras.Interpolate;
import com.dlb.oxygen.extras.LinearInterpolation;

public class DoubleAnimator extends Animator {

    private double value;
    private double start, end;

    private Interpolate interpolate = new LinearInterpolation();

    public DoubleAnimator(double start, double end, double seconds, Runnable finishCallback) {
        super(seconds, finishCallback);
        this.start = start;
        this.end = end;
    }


    public double getValue() {
        return value;
    }

    @Override
    protected void finished() {
        value = end;
    }

    @Override
    protected void animationUpdate(double elapsed, double duration) {
        value = interpolate.interpolate(elapsed, 0, duration, start, end);
    }
}
