package com.dlb.h2o.definitions;

import com.dlb.birne.UIElement;
import com.dlb.birne.UIPanel;
import com.dlb.h2o.definitions.properties.PNinePatchImage;
import com.dlb.oxygen.GameContainer;
import com.dlb.oxygen.extras.Rectangle;

public class DPanel extends DContainer {

    private PNinePatchImage image;

    public static DPanel set() {
        return new DPanel();
    }

    public DPanel image(PNinePatchImage image) {
        this.image = image;
        return this;
    }

    @Override
    public DPanel child(DElement element) {
        return (DPanel) super.child(element);
    }

    @Override
    public DPanel id(String id) {
        return (DPanel) super.id(id);
    }

    @Override
    public DPanel size(double width, double height) {
        return (DPanel) super.size(width, height);
    }

    @Override
    public DPanel position(double x, double y) {
        return (DPanel) super.position(x, y);
    }

    public UIPanel create(UIElement parent, GameContainer gc) {
        UIPanel panel = new UIPanel(id, parent, new Rectangle(x, y, width, height), image.create(gc.assetManager()));
        for (DElement child : children) {
            UIElement element = child.create(parent, gc);
            element.getBounds().move(x, y);
        }
        return panel;
    }
}
