package com.dlb.h2o.definitions;

import com.dlb.birne.UIContainer;
import com.dlb.birne.UIElement;
import com.dlb.oxygen.GameContainer;
import com.dlb.oxygen.extras.Rectangle;

import java.util.ArrayList;
import java.util.List;

public class DContainer extends DElement {
    public static DContainer set() {
        return new DContainer();
    }

    protected List<DElement> children = new ArrayList<>();

    public DContainer id(String id) {
        this.id = id;
        return this;
    }

    public DContainer size(double width, double height) {
        this.width = width;
        this.height = height;
        return this;
    }

    public DContainer position(double x, double y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public DContainer child(DElement element) {
        children.add(element);
        return this;
    }

    public UIContainer create(UIElement parent, GameContainer gc) {
        UIContainer container = new UIContainer(id, parent, new Rectangle(x, y, width, height));
        for (DElement child : children) {
            UIElement element = child.create(container, gc);
            element.getBounds().move(x, y);
        }
        return container;
    }
}
