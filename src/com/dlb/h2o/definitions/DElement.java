package com.dlb.h2o.definitions;

import com.dlb.birne.UIElement;
import com.dlb.oxygen.GameContainer;

public abstract class DElement {

    protected String id;
    protected double x;
    protected double y;
    protected double width;
    protected double height;


    public abstract UIElement create(UIElement parent, GameContainer gc);


}
