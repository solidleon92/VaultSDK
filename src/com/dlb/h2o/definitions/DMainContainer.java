package com.dlb.h2o.definitions;

public class DMainContainer extends DContainer {
    public static DMainContainer set() {
        return new DMainContainer();
    }

    @Override
    public DMainContainer id(String id) {
        return (DMainContainer) super.id(id);
    }

    @Override
    public DMainContainer size(double width, double height) {
        return (DMainContainer) super.size(width, height);
    }

    @Override
    public DMainContainer position(double x, double y) {
        return (DMainContainer) super.position(x, y);
    }

    public DMainContainer child(DElement element) {
        children.add(element);
        return this;
    }

   //public UIMainContainer create(H2O h2O) {
   //    UIMainContainer container = new UIMainContainer(id, h2O, new Rectangle(x, y, width, height));
   //    for (DElement child : children) {
   //        UIElement element = child.create(container);
   //        element.getBounds().move(x, y);
   //    }
   //    return container;
   //}
}
