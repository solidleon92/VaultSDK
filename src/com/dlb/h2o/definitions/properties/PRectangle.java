package com.dlb.h2o.definitions.properties;

import com.dlb.oxygen.extras.Rectangle;

public class PRectangle {

    private double x;
    private double y;
    private double width;
    private double height;

    public PRectangle bounds(double x, double y, double width, double height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        return this;
    }

    public Rectangle create() {
        return new Rectangle(x, y, width, height);
    }

    public static PRectangle set() {
        return new PRectangle();
    }

}
