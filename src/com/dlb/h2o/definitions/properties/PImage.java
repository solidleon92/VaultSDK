package com.dlb.h2o.definitions.properties;

import com.dlb.oxygen.AssetManager;
import com.dlb.oxygen.Image;

public class PImage {

    private String file;

    public static PImage set() {
        return new PImage();
    }

    public PImage file(String file) {
        this.file = file;
        return this;
    }

    public Image create(AssetManager assetManager){
        return assetManager.getImage(file);
    }

}
