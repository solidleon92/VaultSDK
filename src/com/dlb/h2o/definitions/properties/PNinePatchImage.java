package com.dlb.h2o.definitions.properties;

import com.dlb.oxygen.AssetManager;
import com.dlb.oxygen.NinePatchImage;

public class PNinePatchImage {

    private String file;

    public static PNinePatchImage set() {
        return new PNinePatchImage();
    }

    public PNinePatchImage file(String file) {
        this.file = file;
        return this;
    }

    public NinePatchImage create(AssetManager assetManager){
        return new NinePatchImage(assetManager.getImage(file));
    }

}
