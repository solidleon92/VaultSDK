package com.dlb.h2o.definitions;

import com.dlb.birne.UIElement;
import com.dlb.birne.UIText;
import com.dlb.oxygen.GameContainer;
import com.dlb.oxygen.extras.Rectangle;

public class DText extends DElement {
    private String text;

    public static DText set() {
        return new DText();
    }

    public DText id(String id) {
        this.id = id;
        return this;
    }

    public DText position(double x, double y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public DText size(double width, double height) {
        this.width = width;
        this.height = height;
        return this;
    }

    @Override
    public UIText create(UIElement parent, GameContainer gc) {
        UIText uiText = new UIText(id, parent, new Rectangle(x, y, width, height), text);
        return uiText;
    }

    public DText text(String text) {
        this.text = text;
        return this;
    }
}
