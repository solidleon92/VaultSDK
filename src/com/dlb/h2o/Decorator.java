package com.dlb.h2o;

import com.dlb.h2o.definitions.DMainContainer;

public interface Decorator {

    DMainContainer mainContainer(DMainContainer toDecorate);

}
