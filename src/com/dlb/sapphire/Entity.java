package com.dlb.sapphire;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Entity {

    private Map<Class, Object> components = new HashMap<>();

    public Entity() {
    }

    public Entity addComponent(Object component) {
        components.put(component.getClass(), component);
        return this;
    }

    public <T> Optional<T> getComponent(Class<T> aClass) {
        if (components.containsKey(aClass))
            return Optional.of((T)components.get(aClass));
        return Optional.empty();
    }
}
