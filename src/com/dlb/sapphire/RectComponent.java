package com.dlb.sapphire;

import com.dlb.oxygen.Color;

public class RectComponent {
    public Color color = Color.white;

    public RectComponent setColor(Color color) {
        this.color = color;
        return this;
    }
}
