package com.dlb.sapphire;

import com.dlb.oxygen.GameContainer;

public abstract class EntitySystem {
    public abstract void act(GameContainer gc, Entity entity);
}
