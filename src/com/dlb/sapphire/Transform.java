package com.dlb.sapphire;

public class Transform {

    public float x, y;
    public float width, height;

    public Transform(float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
}
