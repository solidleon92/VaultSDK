package com.dlb.sapphire;

import com.dlb.oxygen.GameContainer;

public class RenderSystem extends EntitySystem {
    @Override
    public void act(GameContainer gc, Entity entity) {
            entity.getComponent(Transform.class).ifPresent(transform ->
                    entity.getComponent(RectComponent.class).ifPresent(rectComponent -> {
                        gc.graphics().setColor(rectComponent.color);
                        gc.graphics().fillRect(transform.x, transform.y, transform.width, transform.height);
                    })
            );
    }
}
