package com.dlb.birne;

import com.dlb.oxygen.GameContainer;
import com.dlb.oxygen.extras.Rectangle;

import java.util.ArrayList;
import java.util.List;

public class UIContainer extends UIElement {

    private List<UIElement> children = new ArrayList<>();

    public UIContainer(String id, UIElement parent, Rectangle bounds) {
        super(id, parent, bounds);
    }

    public List<UIElement> getChildren() {
        return children;
    }

    public void add(UIElement element) {
        children.add(element);
    }

    @Override
    public void render(GameContainer gc) {
        children.forEach(element -> element.render(gc));
    }


    public <T extends UIElement> T getElementById(String id) {
        if (id.equals(this.getId()))
            return (T) this;
        for (UIElement child : children) {
            UIElement found = child.getElementById(id);
            if (found != null) return (T) found;
        }
        return null;
    }

    @Override
    public UIElement getElementAt(int x, int y) {
        UIElement found = null;
        if (getBounds().contains(x, y)) {
            found = this;
            for (UIElement child : children) {
                UIElement found2 = child.getElementAt(x, y);
                if (found2 != null) found = found2;
            }
        }
        return found;
    }

    @Override
    public void remove(UIElement element) {
        children.remove(element);
    }
}
