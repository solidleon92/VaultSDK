package com.dlb.birne;

import com.dlb.oxygen.GameContainer;
import com.dlb.oxygen.NinePatchImage;
import com.dlb.oxygen.extras.NinepatchImageRenderer;
import com.dlb.oxygen.extras.Rectangle;

public class UIPanel extends UIContainer {

    private final NinePatchImage image;
    private NinepatchImageRenderer renderer = new NinepatchImageRenderer();

    public UIPanel(String id, UIElement parent, Rectangle bounds, NinePatchImage image) {
        super(id, parent, bounds);
        this.image = image;
    }


    @Override
    public void render(GameContainer gc) {
        renderer.render(gc.graphics(), image, (int)getBounds().getX(), (int)getBounds().getY(), (int)getBounds().getWidth(), (int)getBounds().getHeight());
        super.render(gc);
    }
}
