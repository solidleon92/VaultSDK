package com.dlb.birne;

import com.dlb.oxygen.Color;
import com.dlb.oxygen.Font;
import com.dlb.oxygen.GameContainer;
import com.dlb.oxygen.extras.Rectangle;

public class UIText extends UIElement {

    private String text;

    public UIText(String id, UIElement parent, Rectangle bounds, String text) {
        super(id, parent, bounds);
        this.text = text;
    }


    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void render(GameContainer gc) {
        drawStringCentered(gc, text, getBounds().getXInt(), getBounds().getYInt(), getBounds().getWidthInt(), getBounds().getHeightInt());
    }


    private void drawStringCentered(GameContainer gc, String text, int x, int y, int width, int height) {
        Font fm = gc.graphics().getFont();
        gc.graphics().setColor(Color.white);
        gc.graphics().drawString(text,
                x+ (width - fm.stringWidth(text)) /2f,
                y + (height - fm.getHeight())/2f
        );
    }
    private void drawString(GameContainer gc, String text, int x, int y, int width, int height) {
        gc.graphics().setColor(Color.white);
        gc.graphics().drawString(text,
                x,
                y
        );
    }
}
