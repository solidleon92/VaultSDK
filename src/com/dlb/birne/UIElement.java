package com.dlb.birne;

import com.dlb.oxygen.GameContainer;
import com.dlb.oxygen.extras.Rectangle;

public class UIElement {
    private final String id;
    private final UIElement parent;
    private final Rectangle bounds;

    public UIElement(String id, UIElement parent, Rectangle bounds) {
        this.id = id;
        this.parent = parent;
        this.bounds = bounds;
        if (parent instanceof UIContainer) {
            ((UIContainer) parent).add(this);
        }
    }

    public UIMainContainer getMainContainer() {
        UIElement e = this;
        while (e.parent != null) {
            e = e.parent;
        }
        if (e instanceof UIMainContainer)
            return (UIMainContainer)e;
        throw new IllegalStateException("root is not UIMainContainer, was " + e.getClass().getName());
    }

    public UIElement getParent() {
        return parent;
    }

    public String getId() {
        return id;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void render(GameContainer gc) {

    }

    public UIElement getElementAt(int x, int y) {
        if (getBounds().contains(x, y)) {
            return this;
        }
        return null;
    }

    public <T extends UIElement> T getElementById(String id) {
        return id.equals(this.id) ? (T)this : null;
    }

    public void mousePressed(int button, int x, int y) {

    }

    public void remove() {
        if (parent != null) parent.remove(this);
    }

    public void remove(UIElement element) {
    }

    @Override
    public String toString() {
        return "UIElement{" +
                "id='" + id + '\'' +
                '}';
    }
}
