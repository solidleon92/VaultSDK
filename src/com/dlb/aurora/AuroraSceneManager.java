package com.dlb.aurora;

public class AuroraSceneManager {

    private AuroraScene active;
    private AuroraScene next;

    public void changeScene(AuroraScene next) {
        this.next = next;
    }

    public void applyPendingSceneChange() {
        if (next != null) {
            if (active != null) {
                active.leave();
            }
            active = next;
            next = null;
            if (active != null) {
                active.enter();
            }
        }
    }

}
