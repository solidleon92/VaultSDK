package com.dlb.oxygen;

public interface Font {
    int stringWidth(String text);

    int getHeight();
}
