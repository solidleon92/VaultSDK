package com.dlb.oxygen;

/**
 * Game
 */
public interface Game {
    /**
     * Initialize game here.
     * @param gc
     */
    void init(GameContainer gc);

    void render(GameContainer gc);

    void mouseMoved(int oldx, int oldy, int newx, int newy);

    void mouseDragged(int oldx, int oldy, int newx, int newy);

    void mousePressed(int button, int x, int y, int clickCount);

    void mouseReleased(int button, int x, int y, int clickCount);

}
