package com.dlb.oxygen;

public class Stopwatch {

    private long start;
    private long stop;
    private long lastDeltaTime;

    public void start() {
        start = System.currentTimeMillis();
    }

    public void stop() {
        stop = System.currentTimeMillis();
        lastDeltaTime = stop - start;
    }

    public long getDeltaTime() {
        return stop - start;
    }

    public long getLastDeltaTime() {
        return lastDeltaTime;
    }
}
