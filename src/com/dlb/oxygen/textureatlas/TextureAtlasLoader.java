package com.dlb.oxygen.textureatlas;

import com.dlb.oxygen.AssetManager;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class TextureAtlasLoader {

    public TextureAtlasImage load(AssetManager assetManager, String xmlFile) {

        try (InputStream inputStream = TextureAtlasLoader.class.getResourceAsStream(xmlFile)) {
            TextureAtlas atlas = new Parsing().parse(inputStream);
            String sheet = xmlFile.substring(0, xmlFile.lastIndexOf('.')) + ".png";
            TextureAtlasImage image = new TextureAtlasImage(atlas, assetManager.getImage(sheet));
            return image;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    class Parsing {
        TextureAtlas parse(InputStream inputStream) {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = null;
            MyHandler handler = new MyHandler();
            try {
                parser = factory.newSAXParser();
                parser.parse(inputStream, handler);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return handler.textureAtlas;
        }
    }

    class MyHandler extends DefaultHandler{

        private TextureAtlas textureAtlas;

        public TextureAtlas getTextureAtlas() {
            return textureAtlas;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            super.startElement(uri, localName, qName, attributes);
            if ("TextureAtlas".equals(qName)) {
                textureAtlas = new TextureAtlas(attributes.getValue("imagePath"), new ArrayList<>());
            } else if ("SubTexture".equals(qName)) {
                SubTexture subTexture = new SubTexture(
                        attributes.getValue("name"),
                        Integer.parseInt(attributes.getValue("x")),
                        Integer.parseInt(attributes.getValue("y")),
                        Integer.parseInt(attributes.getValue("width")),
                        Integer.parseInt(attributes.getValue("height"))
                );
                textureAtlas.getSubTextures().add(subTexture);
            }
        }

    }

}
