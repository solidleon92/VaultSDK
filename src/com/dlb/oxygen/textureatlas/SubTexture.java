package com.dlb.oxygen.textureatlas;

import com.dlb.oxygen.Graphics;

public class SubTexture {

    /**
     * Name of this subtexture to select in for example {@link TextureAtlasImage#draw(Graphics, String, double, double)}.
     */
    private String name;

    /**
     * X offset in the image.
     */
    private int x;
    /**
     * Y offset in the image.
     */
    private int y;
    /**
     * Width of the sub texture.
     */
    private int width;
    /**
     * Height of the sub texture.
     */
    private int height;

    public SubTexture(String name, int x, int y, int width, int height) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
