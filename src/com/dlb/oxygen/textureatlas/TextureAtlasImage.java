package com.dlb.oxygen.textureatlas;

import com.dlb.oxygen.Graphics;
import com.dlb.oxygen.Image;

import java.util.Objects;

/**
 * Actual drawable image of a {@link TextureAtlas}.
 */
public class TextureAtlasImage {

    private TextureAtlas textureAtlas;
    private Image image;

    public TextureAtlasImage(TextureAtlas textureAtlas, Image image) {
        this.textureAtlas = Objects.requireNonNull(textureAtlas);
        this.image = Objects.requireNonNull(image);
    }

    /**
     *
     * @param subtextureName SubTexture name {@link SubTexture#name}
     * @return SubTexture with name subtextureName
     * @throws RuntimeException if the subtextureName is not found in {@link TextureAtlas#getSubTextures()}
     */
    public SubTexture get(String subtextureName) {
        for (SubTexture subTexture : textureAtlas.getSubTextures()) {
            if (subTexture.getName().equals(subtextureName))
                return subTexture;
        }
        throw new RuntimeException("SubTexture '"+subtextureName+"' not found!");
    }

    /**
     *
     * @param g graphics to draw into
     * @param subTextureName name of the {@link SubTexture} to draw
     * @param x position
     * @param y position
     */
    public void draw(Graphics g, String subTextureName, double x, double y) {
        draw(g, get(subTextureName), x, y);
    }

    /**
     *
     * @param g graphics to draw into
     * @param subTexture to draw
     * @param x x position
     * @param y y position
     */
    public void draw(Graphics g, SubTexture subTexture, double x, double y) {
        double dx1 = x;
        double dy1 = y;
        double dx2 = dx1 + subTexture.getWidth();
        double dy2 = dy1 + subTexture.getHeight();
        double sx1 = subTexture.getX();
        double sy1 = subTexture.getY();
        double sx2 = sx1 + subTexture.getWidth();
        double sy2 = sy1 + subTexture.getHeight();

        g.drawImage(image, dx1, dy1, dx2, dy2, sx1, sy1, sx2, sy2);


    }
}
