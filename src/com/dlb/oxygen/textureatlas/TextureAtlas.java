package com.dlb.oxygen.textureatlas;

import java.util.List;

public class TextureAtlas {

    /**
     * Path to the image used by this texture atlas.
     */
    private String imagePath;

    /**
     *
     */
    private List<SubTexture> subTextures;

    public TextureAtlas(String imagePath, List<SubTexture> subTextures) {
        this.imagePath = imagePath;
        this.subTextures = subTextures;
    }

    public String getImagePath() {
        return imagePath;
    }

    public List<SubTexture> getSubTextures() {
        return subTextures;
    }

}
