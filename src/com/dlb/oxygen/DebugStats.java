package com.dlb.oxygen;

public class DebugStats {

    private Stopwatch mainLoopTime = new Stopwatch();
    private Stopwatch gameRenderTime = new Stopwatch();
    private Stopwatch displayRenderTime = new Stopwatch();

    public Stopwatch getMainLoopTime() {
        return mainLoopTime;
    }

    public Stopwatch getGameRenderTime() {
        return gameRenderTime;
    }

    public Stopwatch getDisplayRenderTime() {
        return displayRenderTime;
    }
}
