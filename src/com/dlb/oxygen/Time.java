package com.dlb.oxygen;

public class Time {

    private long lastUpdate = 0;
    private double deltaTime;

    public void update(long currentTimestampMs) {
        if (lastUpdate == 0) lastUpdate = currentTimestampMs;
        deltaTime = (currentTimestampMs - lastUpdate) / 1000.0;
        lastUpdate = currentTimestampMs;
    }

    /**
     * @return delta time in seconds
     */
    public double getDeltaTime() {
        return deltaTime;
    }
}
