package com.dlb.oxygen;

/**
 *
 */
public interface GameContainer {

    /**
     *
     * @return graphics for rendering
     */
    Graphics graphics();

    /**
     *
     * @return asset manager
     */
    AssetManager assetManager();

    /**
     *
     * @return game width
     */
    int getWidth();

    /**
     *
     * @return game height
     */
    int getHeight();

    /**
     *
     * @return time
     */
    Time getTime();

    /**
     *
     * @return current mouse x position
     */
    int getMouseX();

    /**
     *
     * @return current mouse y position
     */
    int getMouseY();

    /**
     *
     * @return debug stats
     */
    DebugStats getDebugStats();
}
