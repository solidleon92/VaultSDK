package com.dlb.oxygen.swing;

import com.dlb.oxygen.BaseGameContainer;
import com.dlb.oxygen.Camera;
import com.dlb.oxygen.Game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Core of the Swing implementation.
 *
 * Spins up the drawing canvas.
 * Handles mouse drag.
 * Creates a default font, used to render strings.
 * Starts the main game loop.
 */
public class MainWindow {
    private static final int DISPLAY_WIDTH = 1920;
    private static final int DISPLAY_HEIGHT = 1080;
    private JFrame frame;
    private Canvas canvas;
    private BufferedImage display;
    int oldx, oldy;

    public static Font defaultFont = createDefaultFont();

    private static Font createDefaultFont() {
        try {
            return Font.createFont(Font.TRUETYPE_FONT, MainWindow.class.getResourceAsStream("/kenneynl/uipack/fonts/kenvector_future_thin.ttf")).deriveFont(16f);
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Font("Verdana", Font.PLAIN, 16);
    }

    private BaseGameContainer gameContainer;
    private Game game;

    private Camera camera = new Camera();

    private List<Runnable> onGameLoop = new ArrayList<>();

    public MainWindow(Game game) {
        this.game = game;
        gameContainer = new BaseGameContainer();
        gameContainer.setWidth(DISPLAY_WIDTH);
        gameContainer.setHeight(DISPLAY_HEIGHT);

        frame = new JFrame("Idle Shooter Version 0.0.0");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        createRenderCanvas();
        frame.add(canvas);

        frame.pack();
        frame.setLocationRelativeTo(null);

        createMainLoop();
    }

    private void createMainLoop() {
        new Thread(() -> {
            game.init(gameContainer);
            while (true) {
                gameContainer.getDebugStats().getMainLoopTime().start();
                render();
                gameContainer.getDebugStats().getMainLoopTime().stop();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    private void render() {
        camera.updateScreenSize(canvas.getWidth(), canvas.getHeight());
        synchronized (onGameLoop) {
            onGameLoop.forEach(runnable -> runnable.run());
            onGameLoop.clear();
        }


        gameContainer.getTime().update(System.currentTimeMillis());

        Point mouse = canvas.getMousePosition();
        if (mouse != null) {
            gameContainer.setMouseX((int) camera.toWorldX(mouse.x));
            gameContainer.setMouseY((int) camera.toWorldY(mouse.y));
        }


        Graphics2D g2d = display.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
        g2d.setFont(defaultFont);
        g2d.setColor(Color.black);
        g2d.fillRect(0,0,display.getWidth(), display.getHeight());
        gameContainer.setGraphics(new GraphicsAWT(g2d));
        gameContainer.getDebugStats().getGameRenderTime().start();
        game.render(gameContainer);
        gameContainer.getDebugStats().getGameRenderTime().stop();
        g2d.dispose();


        BufferStrategy bs = canvas.getBufferStrategy();
        if (bs == null) {
            canvas.createBufferStrategy(2);
            return;
        }

        int x = (int) camera.getTranslationX();
        int y = (int) camera.getTranslationY();
        int scaledWidth = camera.getViewportWidth();
        int scaledHeight = camera.getViewportHeight();

        gameContainer.getDebugStats().getDisplayRenderTime().start();
        Graphics2D g = (Graphics2D) bs.getDrawGraphics();
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        g.drawImage(display, x, y, scaledWidth, scaledHeight, null);
        g.dispose();
        bs.show();
        gameContainer.getDebugStats().getDisplayRenderTime().stop();
    }

    private void execute(Runnable runnable) {
        synchronized (onGameLoop) {
            onGameLoop.add(runnable);
        }
    }

    private void createRenderCanvas() {
        display = new BufferedImage(DISPLAY_WIDTH, DISPLAY_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        canvas = new Canvas();
        canvas.setPreferredSize(new Dimension(DISPLAY_WIDTH, DISPLAY_HEIGHT));
        canvas.setIgnoreRepaint(true);

        canvas.addMouseMotionListener(new MouseAdapter() {

            @Override
            public void mouseMoved(MouseEvent e) {
                int newx = (int) camera.toWorldX(e.getX());
                int newy = (int) camera.toWorldY(e.getY());
                execute(() -> game.mouseMoved(oldx, oldy, newx, newy));
                oldx = newx;
                oldy = newy;
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                int newx = (int) camera.toWorldX(e.getX());
                int newy = (int) camera.toWorldY(e.getY());
                execute(() -> game.mouseDragged(oldx, oldy, newx, newy));
                oldx = newx;
                oldy = newy;
            }

        });
        canvas.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int xp = (int) camera.toWorldX(e.getX());
                int yp = (int) camera.toWorldY(e.getY());
                oldx = xp;
                oldy = yp;
                execute(() -> game.mousePressed(e.getButton(), xp, yp, e.getClickCount()));

            }
            @Override
            public void mouseReleased(MouseEvent e) {
                int xp = (int) camera.toWorldX(e.getX());
                int yp = (int) camera.toWorldY(e.getY());
                execute(() -> game.mouseReleased(e.getButton(), xp, yp, e.getClickCount()));

            }
        });
    }

    public JFrame getFrame() {
        return frame;
    }
}
