package com.dlb.oxygen.swing;

import com.dlb.oxygen.extras.Rectangle;

import java.awt.*;

public class GraphicsAWT implements com.dlb.oxygen.Graphics {
    private final Graphics2D graphics;
    private com.dlb.oxygen.Font font;

    public GraphicsAWT(Graphics2D graphics) {
        this.graphics = graphics;
        this.font = new FontAWT(graphics.getFont(), graphics.getFontMetrics());
    }

    @Override
    public com.dlb.oxygen.Font getFont() {
        return font;
    }

    @Override
    public void setColor(com.dlb.oxygen.Color color) {
        graphics.setColor(new java.awt.Color(color.r, color.g, color.b, color.a));
    }

    @Override
    public void drawRect(double x, double y, double width, double height) {
        graphics.drawRect((int) x, (int) y, (int) width, (int) height);
    }
    @Override
    public void fillRect(double x, double y, double width, double height) {
        graphics.fillRect((int) x, (int) y, (int) width, (int) height);
    }

    @Override
    public void drawString(String str, double x, double y) {
        graphics.drawString(str, (int) x, (int) y + graphics.getFontMetrics().getMaxAscent());
    }

    @Override
    public void drawImage(com.dlb.oxygen.Image image, double x, double y) {
        graphics.drawImage(((ImageAWT)image).getImage(), (int)x, (int)y, null);
    }
    @Override
    public void drawImage(com.dlb.oxygen.Image image, double x, double y, double width, double height) {
        graphics.drawImage(((ImageAWT)image).getImage(), (int)x, (int)y, (int) width, (int) height, null);
    }
    @Override
    public void drawImage(com.dlb.oxygen.Image image, double dx1, double dy1, double dx2, double dy2, double sx1, double sy1, double sx2, double sy2) {
        graphics.drawImage(((ImageAWT)image).getImage(), (int)dx1, (int)dy1, (int)dx2, (int)dy2, (int)sx1, (int)sy1, (int)sx2, (int)sy2, null);
    }

    @Override
    public void drawRect(Rectangle bounds) {
        drawRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
    }
    @Override
    public void fillRect(Rectangle bounds) {
        fillRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight());
    }

    @Override
    public void rotate(double theta, double x, double y) {
        graphics.rotate(theta, x, y);
    }

    @Override
    public void dispose() {
        graphics.dispose();
    }

}
