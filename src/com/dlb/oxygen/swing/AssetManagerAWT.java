package com.dlb.oxygen.swing;

import com.dlb.oxygen.AssetManager;
import com.dlb.oxygen.Image;
import com.dlb.oxygen.NullSound;
import com.dlb.oxygen.Sound;
import com.dlb.oxygen.extras.SimplexNoise;
import com.dlb.oxygen.textureatlas.TextureAtlasImage;
import com.dlb.oxygen.textureatlas.TextureAtlasLoader;

import java.util.HashMap;
import java.util.Map;

public class AssetManagerAWT implements AssetManager {
    private Map<String, Image> images = new HashMap<>();
    private Map<String, TextureAtlasImage> textureAtlasImageMap = new HashMap<>();

    private TextureAtlasLoader textureAtlasLoader = new TextureAtlasLoader();

    @Override
    public Image getImage(String ref) {
        return images.computeIfAbsent(ref, ImageAWT::new);
    }

    @Override
    public void putImage(String ref, Image image) {
        images.put(ref, image);
    }

    @Override
    public Image createImage(int width, int height) {
        return new ImageAWT(width, height);
    }

    @Override
    public Image loadImage(String file) {
        return new ImageAWT(file);
    }

    @Override
    public TextureAtlasImage loadTextureAtlas(String xmlFile) {
        return textureAtlasImageMap.computeIfAbsent(xmlFile, s -> textureAtlasLoader.load(this, s));
    }

    @Override
    public Image createNoiseTexture(int w, int h, int numInterations, double persistence, double scale, double low, double high, double r, double g, double b) {
        Image image =  createImage(w, h);
        for (int y = 0; y < w; y++) {
            for (int x = 0; x < h; x++) {
                double noise = SimplexNoise.sumOctave(numInterations, x, y, persistence, scale, low, high) / high;
                image.setARGB(x, y, noise * r, noise * g, noise * b, 1.0);
            }
        }
        return image;
    }


    @Override
    public Sound loadSound(String ref) {
        return NullSound.instance;
    }
}
