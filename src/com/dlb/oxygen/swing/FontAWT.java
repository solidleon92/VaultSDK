package com.dlb.oxygen.swing;

import java.awt.*;

public class FontAWT implements com.dlb.oxygen.Font {

    private java.awt.Font font;
    private FontMetrics fm;

    public FontAWT(java.awt.Font font, FontMetrics fm) {
        this.font = font;
        this.fm = fm;
    }

    @Override
    public int stringWidth(String text) {
        return fm.stringWidth(text);
    }

    @Override
    public int getHeight() {
        return fm.getHeight();
    }
}
