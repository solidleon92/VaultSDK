package com.dlb.oxygen.swing;

import com.dlb.oxygen.Graphics;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class ImageAWT implements com.dlb.oxygen.Image {

    private final BufferedImage image;

    public ImageAWT(int width, int height) {
        System.out.println("NEW IMAGE " + width + "/" + height);
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    }

    public ImageAWT(String ref) {
        System.out.println("NEW IMAGE " + ref);
        BufferedImage loaded = null;
        try {
            loaded = ImageIO.read(ImageAWT.class.getResourceAsStream(ref));
        } catch (Exception e) {
            try {
                loaded = ImageIO.read(new File(ref));
            } catch (Exception e1) {
                System.err.println("Cannot load image: " + ref);
                throw new RuntimeException(e);
            }
        }
        image = loaded;
    }

    @Override
    public Graphics createGraphics() {
        Graphics2D g2d = image.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
        g2d.setFont(MainWindow.defaultFont);
        return new GraphicsAWT(g2d);
    }

    public BufferedImage getImage() {
        return image;
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }


    public static BufferedImage toBufferedImage(java.awt.Image img)
    {
        if (img instanceof BufferedImage)
        {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }

    @Override
    public void setARGB(int x, int y, int argb) {
        image.setRGB(x, y, argb);
    }

    @Override
    public void setARGB(int x, int y, double r, double g, double b, double a) {
        byte br = (byte) (r * 255.0);
        byte bg = (byte) (g * 255.0);
        byte bb = (byte) (b * 255.0);
        byte ba = (byte) (a * 255.0);
        int argb = ((ba&0xFF) << 24) | ((br&0xFF) << 16) | ((bg&0xFF) << 8) | ((bb&0xFF));
        setARGB(x, y, argb);
    }

    @Override
    public int getARGB(int x, int y) {
        return image.getRGB(x, y);
    }

    @Override
    public com.dlb.oxygen.Image getScaledCopy(double scale) {
        int w = (int) (getWidth() * scale);
        int h = (int) (getHeight() * scale);
        ImageAWT scaled = new ImageAWT(w, h);
        Graphics2D g = scaled.image.createGraphics();
        g.drawImage(image, 0, 0, w, h, null);
        g.dispose();
        return scaled;
    }
}
