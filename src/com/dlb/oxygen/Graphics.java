package com.dlb.oxygen;

import com.dlb.oxygen.extras.Rectangle;

public interface Graphics {
    Font getFont();

    void setColor(Color color);

    void drawRect(double x, double y, double width, double height);

    void fillRect(double x, double y, double width, double height);

    void drawString(String str, double x, double y);

    void drawImage(Image image, double x, double y);

    void drawImage(Image image, double x, double y, double width, double height);

    void drawImage(Image image, double dx1, double dy1, double dx2, double dy2, double sx1, double sy1, double sx2, double sy2);

    void drawRect(Rectangle bounds);

    void fillRect(Rectangle bounds);

    void rotate(double theta, double x, double y);

    void dispose();
}
