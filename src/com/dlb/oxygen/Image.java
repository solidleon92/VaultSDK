package com.dlb.oxygen;

public interface Image {
    /**
     *
     * @return graphics to draw into this image
     */
    Graphics createGraphics();

    /**
     *
     * @return image width
     */
    int getWidth();

    /**
     *
     * @return image height
     */
    int getHeight();


    /**
     *
     * @param x pixel x coordinate
     * @param y pixel y coordinate
     * @param argb alpha-red-green-blue color value
     */
    void setARGB(int x, int y, int argb);

    /**
     *
     * @param x pixel x coordinate
     * @param y pixel y coordinate
     * @param r red color (0.0 - 1.0)
     * @param g green color (0.0 - 1.0)
     * @param b blue color (0.0 - 1.0)
     * @param a alpha color (0.0 - 1.0)
     */
    void setARGB(int x, int y, double r, double g, double b, double a);

    /**
     *
     * @param x pixel x coordinate
     * @param y pixel y coordinate
     * @return alpha-red-green-blue color
     */
    int getARGB(int x, int y);

    /**
     * Creates a new image, scaling width and height of this image.
     * @param scale factor
     * @return scaled image
     */
    Image getScaledCopy(double scale);
}
