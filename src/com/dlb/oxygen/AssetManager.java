package com.dlb.oxygen;

import com.dlb.oxygen.textureatlas.TextureAtlasImage;

public interface AssetManager {

    /**
     * Accesses the specified resource.
     * If not present, the resource is loaded.
     * @param ref - resource to access
     * @return Image
     */
    Image getImage(String ref);

    /**
     *
     * @param ref resource name to add
     * @param image image to add
     */
    void putImage(String ref, Image image);

    /**
     * Creates an new empty image.
     * @param width desired width
     * @param height desired height
     * @return Image
     */
    Image createImage(int width, int height);

    /**
     * Loads an image without caching.
     * @param file - resource to load
     * @return Image
     */
    Image loadImage(String file);

    /**
     *
     * @param xmlFile xml resource to load
     * @return TextureAtlasImage
     */
    TextureAtlasImage loadTextureAtlas(String xmlFile);

    /**
     * Creates a new {@link Image} using {@link com.dlb.oxygen.extras.SimplexNoise} to create a procedurally generated
     * image.
     * @param w desired width
     * @param h desired height
     * @param numInterations octave iterations
     * @param persistence ???
     * @param scale x/y scale factor
     * @param low low amplitude
     * @param high high amplitude
     * @param r red coefficient
     * @param g green coefficient
     * @param b blue coefficient
     * @return Image
     */
    Image createNoiseTexture(int w, int h, int numInterations, double persistence, double scale, double low, double high, double r, double g, double b);

    /**
     * Loads the sound.
     * @param ref resource to load
     * @return Sound
     */
    Sound loadSound(String ref);
}
