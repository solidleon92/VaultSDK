package com.dlb.oxygen.extras;

public class Rectangle {

    private double x;
    private double y;
    private double width;
    private double height;



    public Rectangle(double x, double y, double width, double height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public Rectangle set(double x, double y, double width, double height) {
        this.x = x;
        this.y = y;
        this.width =width;
        this.height = height;
        return this;
    }

    public int getXInt() { return (int) x; }
    public int getYInt() { return (int) y; }
    public int getWidthInt() { return (int) width; }
    public int getHeightInt() { return (int) height; }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double getMinX() { return x; }
    public double getMaxX() { return x + width; }
    public double getMinY() { return y; }
    public double getMaxY() { return y + height; }
    public double getCenterX() { return x + width/2.0; }
    public double getCenterY() { return y + height/2.0; }


    public boolean contains(double xp, double yp) {
        return xp >= getMinX() && xp < getMaxX() && yp >= getMinY() && yp < getMaxY();
    }

    public Rectangle scale(double scale) {
        x *= scale;
        y *= scale;
        width *= scale;
        height *= scale;
        return this;
    }

    public Rectangle move(double dx, double dy) {
        x += dx;
        y += dy;
        return this;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    public Rectangle copy() {
        return new Rectangle(x, y, width, height);
    }

    public void position(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
