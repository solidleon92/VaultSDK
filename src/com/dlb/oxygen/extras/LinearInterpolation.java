package com.dlb.oxygen.extras;

public class LinearInterpolation implements Interpolate{

    public double interpolate(double x, double xa, double xb, double ya, double yb) {
        return ya + (yb - ya) * ((x - xa) / (xb - xa));
    }

}
