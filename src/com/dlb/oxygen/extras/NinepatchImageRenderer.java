package com.dlb.oxygen.extras;

import com.dlb.oxygen.Graphics;
import com.dlb.oxygen.Image;
import com.dlb.oxygen.NinePatchImage;

public class NinepatchImageRenderer {

    public void render(Graphics g, NinePatchImage ninePatchImage, int x, int y, int width, int height) {
        Image image = ninePatchImage.getImage();
        int cornerWidth = 8;
        int cornerHeight = 8;
        // top left
        g.drawImage(image,
                x, y, x + cornerWidth, y+ cornerHeight,
                0, 0, cornerWidth, cornerHeight);

        // top right
        g.drawImage(image,
                x + width - cornerWidth, y, x + width, y+ cornerHeight,
                image.getWidth() - cornerWidth, 0, image.getWidth(), cornerHeight);

        // bottom left
        g.drawImage(image,
                x, y+height-cornerHeight, x + cornerWidth, y+height,
                0, image.getHeight() - cornerHeight, cornerWidth, image.getHeight());

        // bottom right
        g.drawImage(image,
                x + width - cornerWidth, y+height-cornerHeight, x + width, y+height,
                image.getWidth() - cornerWidth, image.getHeight() - cornerHeight, image.getWidth(), image.getHeight());

        // top
        g.drawImage(image,
                x + cornerWidth, y, x + width - cornerWidth, y + cornerHeight,
                cornerWidth, 0, cornerWidth + 8, cornerHeight);


        // bottom
        g.drawImage(image,
                x + cornerWidth, y + height - cornerHeight, x + width - cornerWidth, y + height,
                cornerWidth, image.getHeight() - cornerHeight, cornerWidth + 8, image.getHeight());


        // left
        g.drawImage(image,
                x, y + cornerHeight, x + cornerWidth, y + height - cornerHeight,
                0, cornerHeight, 8, 2*cornerHeight);

        // right
        g.drawImage(image,
                x + width - cornerWidth, y + cornerHeight, x + width, y + height - cornerHeight,
                image.getWidth() - cornerWidth, cornerHeight, image.getWidth(), 2*cornerHeight);

        // center

        g.drawImage(image,
                x + cornerWidth, y + cornerHeight, x + width - cornerWidth, y + height - cornerHeight,
                cornerWidth, cornerHeight, cornerWidth*2, cornerHeight*2
        );
    }
}
