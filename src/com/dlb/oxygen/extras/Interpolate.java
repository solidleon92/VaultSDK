package com.dlb.oxygen.extras;

public interface Interpolate {

    double interpolate(double x, double x0, double x1, double y0, double y1);
}
