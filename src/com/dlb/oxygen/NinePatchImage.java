package com.dlb.oxygen;

public class NinePatchImage {
    private Image image;

    public NinePatchImage(Image image) {
        this.image = image;
    }

    public Image getImage() {
        return image;
    }
}
