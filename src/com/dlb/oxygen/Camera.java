package com.dlb.oxygen;

/**
 * Simple 2D orthogonal camera.
 * This camera uses a fixed resolution of 1920x1080, and scales the content so that it fits, while keeping the aspect ratio.
 */
public class Camera {

    private int screenWidth;
    private int screenHeight;
    private int worldWidth = 1920;
    private int worldHeight = 1080;
    private int viewportWidth;
    private int viewportHeight;
    private double scale = 1.0;
    private double translationX = 0;
    private double translationY = 0;

    public double getTranslationX() {
        return translationX;
    }

    public double getTranslationY() {
        return translationY;
    }

    public double toWorldX(double x) {
        return (x - translationX) / scale;
    }
    public double toScreenX(double x) {
        return (x * scale) + translationX;
    }
    public double toWorldY(double x) {
        return (x - translationY) / scale;
    }
    public double toScreenY(double x) {
        return (x * scale) + translationY;
    }

    public void updateScreenSize(int screenWidth, int screenHeight) {
        if (this.screenWidth != screenWidth || this.screenHeight != screenHeight) {
            this.screenWidth = screenWidth;
            this.screenHeight = screenHeight;

            int scaledWidth = worldWidth;
            int scaledHeight = worldHeight;
            double pWidth = screenWidth / (double) worldWidth;
            double pHeight = screenHeight / (double) worldHeight;
            if (pWidth < 1 || pHeight < 1) {
                scale = pWidth < pHeight ? pWidth : pHeight;
                scaledWidth = (int) (worldWidth * scale);
                scaledHeight = (int) (worldHeight * scale);
            }

            translationX = ((screenWidth - scaledWidth) / 2.0);
            translationY = ((screenHeight - scaledHeight) / 2.0);

            viewportWidth = scaledWidth;
            viewportHeight = scaledHeight;
        }
    }

    public int getViewportWidth() {
        return viewportWidth;
    }

    public int getViewportHeight() {
        return viewportHeight;
    }
}
