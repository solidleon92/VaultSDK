package com.dlb.oxygen;

public class Color {
    public static Color white = new Color(0xFFFFFFFF);
    public static Color BLACK = new Color(0xFF);
    public static Color black = new Color(0xFF);
    public static Color red = new Color(0xFF0000FF);
    public static Color green = new Color(0x00FF00FF);
    public static Color blue = new Color(0x0000FFFF);

    public final float r, g, b, a;

    public Color(float r, float g, float b, float a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }
    public Color(int r, int g, int b, int a) {
        this.r = r/255f;
        this.g = g/255f;
        this.b = b/255f;
        this.a = a/255f;
    }

    public Color(int rgba) {
        this.a = (rgba&0xFF) / 255.0f;
        this.b = ((rgba>>8)&0xFF) / 255.0f;
        this.g = ((rgba>>16)&0xFF) / 255.0f;
        this.r = ((rgba>>24)&0xFF) / 255.0f;
    }




}
