package com.dlb.oxygen;

import com.dlb.oxygen.swing.AssetManagerAWT;

public class BaseGameContainer implements GameContainer{
    private Graphics graphics;
    private Time time = new Time();
    protected int width, height;
    protected int mouseX, mouseY;
    private DebugStats debugStats = new DebugStats();
    private AssetManager assetManager = new AssetManagerAWT();


    public DebugStats getDebugStats() {
        return debugStats;
    }

    public void setMouseX(int mouseX) {
        this.mouseX = mouseX;
    }

    public void setMouseY(int mouseY) {
        this.mouseY = mouseY;
    }

    @Override
    public int getMouseX() {
        return mouseX;
    }

    @Override
    public int getMouseY() {
        return mouseY;
    }

    @Override
    public Graphics graphics() {
        return graphics;
    }

    @Override
    public AssetManager assetManager() {
        return assetManager;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public int getWidth() {
        return width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public Time getTime() {
        return time;
    }

    public void setGraphics(Graphics graphics) {
        this.graphics = graphics;
    }
}
