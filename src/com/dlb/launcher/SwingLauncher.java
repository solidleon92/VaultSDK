package com.dlb.launcher;

import com.dlb.oxygen.Game;
import com.dlb.oxygen.swing.MainWindow;

import javax.swing.*;

public class SwingLauncher {

    private final Game game;

    public SwingLauncher(Game game) {
        this.game = game;
    }


    public void start() {
        SwingUtilities.invokeLater(() -> new MainWindow(game).getFrame().setVisible(true));
    }
}
